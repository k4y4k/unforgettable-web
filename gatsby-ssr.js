const React = require('react')
const { Provider } = require('react-redux')
const store = require('./src/redux/createStore').default

exports.wrapRootElement = ({ element }) => {
  return <Provider store={store}>{element}</Provider>
}
