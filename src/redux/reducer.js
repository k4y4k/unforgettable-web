/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

const mainReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_CHARACTER_SELECTION':
      return {
        ...state,
        currentlySelectedAnimation: action.animationName,
      }

    default:
      return state
  }
}

export default mainReducer
