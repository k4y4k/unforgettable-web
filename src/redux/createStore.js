/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import allAnimations from '../anim/animExport.ts'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore } from 'redux'
import reducer from './reducer'
import supplementaryData from '../data/supplementary-data.json'

const composedEnhancers = composeWithDevTools()

const initialState = {
  currentlySelectedAnimation: null,
  // only need the names of the animations
  animationsPool: Object.keys(allAnimations),
  customSpinOptions: {
    noDuplicates: false,
    removeAfterSpin: false,
    noRecurring: false,
    chronologicallyFirstOnly: false,
  },
  supplementaryData,
}

const store = createStore(reducer, initialState, composedEnhancers)

export default store
