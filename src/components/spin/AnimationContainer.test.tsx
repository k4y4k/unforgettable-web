import * as React from 'react'
import AnimationContainer from './AnimationContainer'
import { render } from '../../../jest/mock-connected-components.js'

describe('<AnimationContainer />', () => {
  test('renders OK', () => {
    const { container } = render(
      <AnimationContainer
        animName='Alessander'
        name='Alessander'
        frameData={[
          [0, 1],
          [2, 3],
        ]}
      />,
      { initialState: { currentlySelectedAnimation: 'Alessander' } }
    )

    expect(container).toMatchSnapshot()
  })

  test('returns null if passed null', () => {
    const { container } = render(
      <AnimationContainer
        animName='this-doesnt-exist'
        name=''
        title=''
        frameData={[
          [0, 1],
          [2, 3],
        ]}
      />,
      { initialState: { currentlySelectedAnimation: 'this-doesnt-exist' } }
    )

    expect(container).toMatchSnapshot()
  })
})
