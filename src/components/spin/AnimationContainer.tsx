import * as React from 'react'
import tw, { css } from 'twin.macro'
import getJson from '../../helpers/getAnimationJsonPathFromName'
import Lottie from 'react-lottie-player'
import Nameplate from '../ui/Nameplate'

interface animatonContainerTypes {
  animName: string
  name: string
  title?: string
  frameData: [
    // introFrames:
    [number, number],
    // loopFrames:
    [number, number]
  ]
}

const animContainerStyles = css`
  ${tw`mx-auto overflow-hidden relative`}
`

const animStyles = css`
  svg {
    ${tw`h-screen! w-auto! relative! left-2/4!`}
    transform: translateX(-50%) !important;
  }
`

const AnimationContainer = ({
  animName,
  name,
  title,
  frameData,
}: animatonContainerTypes): JSX.Element | null => {
  const data = getJson(animName)
  if (data === null) return null

  return (
    <div data-testid='animation-container' css={animContainerStyles}>
      <Lottie
        loop
        play
        animationData={data}
        renderer='svg'
        css={animStyles}
        style={{ zIndex: '-99' }}
        rendererSettings={{
          progressiveLoad: true,
          imagePreserveAspectRatio: 'xMidYMid slice',
        }}
        data-testid='lottie-container'
        segments={frameData}
      />
      <Nameplate name={name} title={title} />
    </div>
  )
}

export default AnimationContainer
