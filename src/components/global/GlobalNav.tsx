import * as React from 'react'
import tw, { css, GlobalStyles } from 'twin.macro'
import Button from '../ui/Button'
import repickCharacters from '../../helpers/repickCharacters'

export interface GlobalNavTypes {
  location: Record<string, unknown>
}

const navStyles = css`
  ${tw`absolute top-0 left-0`}
  z-index: 99;
`

const GlobalNav = ({ location }: GlobalNavTypes): JSX.Element => {
  return (
    <>
      <GlobalStyles />

      {location.pathname !== '/' ? (
        <nav data-testid='globalNavigation' css={navStyles}>
          <Button to='/' text={'BACK'} />
          <Button onClick={repickCharacters} text={'AGAIN'} />
        </nav>
      ) : null}
    </>
  )
}

export default GlobalNav
