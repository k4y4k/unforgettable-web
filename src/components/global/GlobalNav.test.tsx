import * as React from 'react'
import { render, screen } from '@testing-library/react'
import GlobalNav from './GlobalNav'

describe('<GlobalNav />', () => {
  const location = {
    pathname: '/somepath',
  }

  test('displays BACK and AGAIN', () => {
    render(<GlobalNav location={location} />)

    expect(screen.getByText('BACK')).toBeInTheDocument()
    expect(screen.getByText('AGAIN')).toBeInTheDocument()

    expect(screen.getByTestId('globalNavigation')).toMatchSnapshot()
  })

  test("doesn't display BACK or AGAIN on the home page", () => {
    render(<GlobalNav location={{ ...location, pathname: '/' }} />)

    expect(screen.queryByText('BACK')).not.toBeInTheDocument()
    expect(screen.queryByText('AGAIN')).not.toBeInTheDocument()

    expect(screen.queryByTestId('globalNavigation')).toMatchSnapshot()
  })
})
