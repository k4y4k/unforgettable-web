import * as React from 'react'
import tw, { css } from 'twin.macro'

interface NameplateTypes {
  name: string
  title?: string
}

const nameplateStyles = css`
  ${tw`bg-green-400 hover:bg-green-700 absolute px-4 py-2 text-center bottom-0 left-1/2 z-0`}
  transform: translate(-50%, 0);
`

const nameStyles = css`
  ${tw`text-3xl font-bold`}
`

const titleStyles = css`
  ${tw`text-lg uppercase font-black`}
`

const Nameplate = ({ name, title }: NameplateTypes): JSX.Element => (
  <div css={nameplateStyles}>
    <h1 css={nameStyles}>{name}</h1>
    <h2 css={titleStyles}>{title}</h2>
  </div>
)

export default Nameplate
