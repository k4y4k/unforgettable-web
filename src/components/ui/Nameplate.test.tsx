import * as React from 'react'
import { render, screen } from '@testing-library/react'
import Nameplate from './Nameplate'

describe('<Nameplate />', () => {
  test('displays name', () => {
    const { container } = render(<Nameplate name='Character Name' />)
    const name = screen.getByText('Character Name')

    expect(name).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  test('displays name and title', () => {
    const { container } = render(
      <Nameplate name='Character Name' title='Important Title' />
    )
    const name = screen.getByText('Character Name')
    const title = screen.getByText('Important Title')

    expect(name).toBeInTheDocument()
    expect(title).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
