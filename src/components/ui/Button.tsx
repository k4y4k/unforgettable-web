import * as React from 'react'
import tw, { css } from 'twin.macro'
import { Link } from 'gatsby'

interface ButtonTypes {
  text: string
  danger?: boolean
  disabled?: boolean | undefined
  to?: string
  children?: React.ReactChild[]
  onClick?: () => void
}

const buttonStyles = css`
  ${tw`p-2 rounded-lg text-black font-bold shadow-sm border m-2`}
  ${tw`hover:(text-white shadow-md)`}
  ${tw`bg-green-400 border-green-700`}
  ${tw`hover:(bg-green-700  border-green-400)`}

  a {
    ${tw`block m-0 p-2`}
  }
`

const dangerStyles = css`
  ${tw`bg-purple-400  border-purple-700`}
  ${tw`hover:bg-purple-700  border-purple-400`}
`

const disabledStyles = css`
  ${tw`bg-gray-300 border-gray-500 cursor-not-allowed`}
  ${tw`hover:(bg-gray-300 border-gray-500 text-black)`}
`

const Button = ({
  text,
  danger = false,
  disabled = false,
  to,
  children,
  onClick,
}: ButtonTypes): JSX.Element => {
  // if we pass in a `to` prop:
  if (to !== undefined && disabled !== null && !disabled)
    // if the button is NOT also disabled
    return (
      <button
        css={[
          css`
            ${tw`p-0!`}
          `,
          buttonStyles,
          danger && dangerStyles,
        ]}
      >
        <Link to={to}>{children ?? text}</Link>
      </button>
    )

  return (
    <button
      disabled={disabled}
      onClick={onClick}
      css={[
        buttonStyles,
        danger && dangerStyles,
        disabled !== null && disabled && disabledStyles,
      ]}
    >
      {children ?? text}
    </button>
  )
}

export default Button
