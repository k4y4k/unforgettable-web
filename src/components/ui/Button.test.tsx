import * as React from 'react'
import Button from './Button'
import { render } from '@testing-library/react'

describe('<Button />', () => {
  test('renders OK', () => {
    const { container } = render(<Button text='Woomy' />)
    expect(container).toMatchSnapshot()
  })

  test('disabled styles', () => {
    const { container } = render(<Button disabled text='danger' />)
    expect(container).toMatchSnapshot()
  })

  test('danger styles', () => {
    const { container } = render(<Button danger text='danger' />)
    expect(container).toMatchSnapshot()
  })

  test('danger styles that go places', () => {
    const { container } = render(<Button to='/' danger text='danger' />)
    expect(container).toMatchSnapshot()
  })
})
