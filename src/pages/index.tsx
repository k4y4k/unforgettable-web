import * as React from 'react'
import tw, { css, styled } from 'twin.macro'
import Button from '../components/ui/Button'
import GlobalNav from '../components/global/GlobalNav'
import repickCharacters from '../helpers/repickCharacters'

interface IndexTypes {
  location: Record<string, unknown>
}

const containerStyles = css`
  ${tw`w-screen h-screen flex justify-center items-center`}
`

const Title = styled.h1`
  ${tw`text-5xl font-bold mb-8 leading-snug`}
`

const indexStyles = css`
  ${tw`text-center flex flex-col items-center`}
  max-width: 1200px;
`

const IndexPage = ({ location }: IndexTypes): JSX.Element => {
  // prepopulate the spin pages, else it crashes on arrival
  repickCharacters()

  return (
    <>
      <div css={containerStyles}>
        <div css={indexStyles}>
          <GlobalNav location={location} />
          <Title>
            The Indescribable, Unforgettable, Unstoppable, Uncontainable and
            Thoroughly, Intrinsically, Very Very Remarkable Disaster Wheel
          </Title>

          <Button to='/spin' text='spin (1)' />

          <Button disabled text='spin (3)' />

          <Button disabled text='8 Player Smash' />
        </div>
      </div>
    </>
  )
}

export default IndexPage
