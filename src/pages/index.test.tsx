import * as React from 'react'
import { render, screen } from '@testing-library/react'
import Index from './index'

describe('index page', () => {
  test('renders OK', () => {
    const { container } = render(<Index location={{ pathname: '/' }} />)

    expect(
      screen.getByText('disaster wheel', { exact: false })
    ).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
