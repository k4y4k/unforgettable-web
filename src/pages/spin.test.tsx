import * as React from 'react'
import { render } from '../../jest/mock-connected-components.js'
import SpinPage from './spin'
import supplementaryData from '../data/supplementary-data.json'

describe('/spin', () => {
  test('renders OK', () => {
    const { container } = render(
      <SpinPage location={{ pathname: '/spin' }} />,
      {
        initialState: {
          currentlySelectedAnimation: 'Alessander',
          supplementaryData,
        },
      }
    )

    expect(container).toMatchSnapshot()
  })
})
