import * as React from 'react'
import tw, { css } from 'twin.macro'
import AnimationContainer from '../components/spin/AnimationContainer'
import { connect } from 'react-redux'
import GlobalNav from '../components/global/GlobalNav'

interface SpinTypes {
  location: Record<string, unknown>
  animName: string
  supplementaryData: supplementaryItem[]
}

interface supplementaryItem {
  name: string
  title?: string
  animationNames: string[]
  frameData: [[number, number], [number, number]]
}

const spinStyles = css`
  ${tw`flex flex-row flex-nowrap`}
`

const mapStateToProps = (state: {
  currentlySelectedAnimation: string
  supplementaryData: supplementaryItem[]
}): { animName: string; supplementaryData: supplementaryItem[] } => {
  return {
    animName: state.currentlySelectedAnimation,
    supplementaryData: state.supplementaryData,
  }
}

const SpinPage = ({
  location,
  animName,
  supplementaryData,
}: SpinTypes): JSX.Element | null => {
  // in the (unlikely, but possible) case that /spin is loaded without a
  // selected animation in the store, choose a random one
  const thisCharacter =
    supplementaryData.filter(el => el.animationNames.includes(animName))[0] ??
    supplementaryData[Math.floor(Math.random() * supplementaryData.length)]

  return (
    <>
      <GlobalNav location={location} />

      <div css={spinStyles}>
        <AnimationContainer
          animName={animName}
          name={thisCharacter.name}
          title={thisCharacter.title}
          frameData={thisCharacter.frameData}
        />
      </div>
    </>
  )
}

export default connect(mapStateToProps)(SpinPage)
