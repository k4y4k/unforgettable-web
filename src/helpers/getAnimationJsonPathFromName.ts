// getAnimationJsonFromName.ts

// input: a kebab-case filename, including extension

// output: a JSON object for consumption by react-lottie-player

import animationDataCollection from '../anim/animExport'
import { pascalCase } from 'pascal-case'

const getAnimationJsonFromName = (
  animName: string
): Record<string, unknown> | null => {
  if (animName === undefined)
    throw new Error("something's up with getAnimationJsonFromName()")

  if (animName === null) return null

  // transform name for lookup
  const lookingFor = pascalCase(animName)

  // return lookup json
  const res = animationDataCollection[lookingFor]

  // escape hatch to prevent returning undefined
  if (res === undefined) return null

  return res
}

export default getAnimationJsonFromName
