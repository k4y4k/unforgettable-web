// picks new characters from pool
// input: pool
// output: animation name string

import store from '../redux/createStore.js'

export const repickCharacters = (): void => {
  const available = [...store.getState().animationsPool]
  const randomPick = available[Math.floor(Math.random() * available.length)]

  // reflect new pick in Redux state
  return store.dispatch({
    type: 'CHANGE_CHARACTER_SELECTION',
    animationName: randomPick,
  })
}

export default repickCharacters
