import getAnimationJsonPathFromName from './getAnimationJsonPathFromName'

describe('getAnimationJsonPathFromName', () => {
  test('throws errors if it trips up or gets bad data', () => {
    // @ts-expect-error: not supposed to manually pass in undefined
    expect(() => getAnimationJsonPathFromName(undefined)).toThrow()
  })

  test('return null if passed null', () => {
    // @ts-expect-error: not supposed to manually pass in null
    expect(getAnimationJsonPathFromName(null)).toBeNull()
  })

  test("return null if there isn't a JSON file for a name", () => {
    expect(getAnimationJsonPathFromName('ThisDoesntExist')).toBeNull()
  })
})
