<!-- -->

<div align="center">

# The Indescribable, Unforgettable, Unstoppable, Uncontainable and Thoroughly, Intrinsically, Very Very Remarkable Disaster Wheel

(or just 'The Disaster Wheel' for short)

![](https://img.shields.io/badge/documented%20with-storybook-blue?style=for-the-badge&logo=storybook&logoColor=white) ![](https://img.shields.io/badge/built%20with-bors-blue?style=for-the-badge&logoColor=white) ![](https://img.shields.io/badge/tested%20with-jest-blue?style=for-the-badge&logo=jest&logoColor=white) ![](https://img.shields.io/badge/tested%20with-react%20testing%20library-blue?style=for-the-badge&logo=testing-library&logoColor=white) ![](https://img.shields.io/badge/linted%20with-eslint-blue?style=for-the-badge&logo=eslint&logoColor=white) ![](https://img.shields.io/badge/formatted%20with-prettier-blue?style=for-the-badge&logo=prettier&logoColor=white) ![](https://img.shields.io/badge/uses-gatsby-purple?style=for-the-badge&logo=gatsby) ![](https://img.shields.io/badge/uses-typescript-purple?style=for-the-badge&logo=typescript&logoColor=white) ![](https://img.shields.io/badge/uses-tailwindcss-purple?style=for-the-badge&logoColor=white) ![](https://img.shields.io/badge/renovate-enabled-blue?style=for-the-badge&logo=renovatebot&logoColor=white)

</div>
